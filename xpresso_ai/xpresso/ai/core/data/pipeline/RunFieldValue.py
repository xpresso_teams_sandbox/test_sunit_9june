from enum import  Enum


class RunFieldValue(Enum):
    """
    Enum class to standardize all the database collection values(predefined)
    for certain fields
    """

    RUN_STATUS_RUNNING = "RUNNING"
    RUN_STATUS_IDLE = "IDLE"
    RUN_STATUS_RESTART = "RESTARTED"
    RUN_STATUS_PAUSED = "PAUSED"
    RUN_STATUS_TERMINATE = "TERMINATED"
    COMPONENT_STATUS_COMPLETED = "COMPONENT_COMPLETED"
    RUN_STATUS_COMPLETED = "COMPLETED"
    RUN_STATUS_FAILED = "FAILED"
    def __str__(self):
        return self.value
